import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questionText': 'What\'s your favorite color',
      'answers': [
        {'text': 'Black', 'score': 10},
        {'text': 'Red', 'score': 5},
        {'text': 'Green', 'score': 8},
        {'text': 'White', 'score': 1}
      ]
    },
    {
      'questionText': 'What Your Favorite Animal',
      'answers': [
        {'text': 'Dog', 'score': 4},
        {'text': 'Cat', 'score': 10},
        {'text': 'Rabbit', 'score': 1},
        {'text': 'Snake', 'score': 7}
      ]
    },
    {
      'questionText': 'Who Is Your Favorite Mentor',
      'answers': [
        {'text': 'Chriss', 'score': 4},
        {'text': 'John', 'score': 10},
        {'text': 'Doe', 'score': 1}
      ]
    }
  ];
  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetPlay() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestion(int score) {
    _totalScore += score;
    setState(() {
      _questionIndex = _questionIndex + 1;
    });
    print(_questionIndex);
    if (_questionIndex < _questions.length) {
      print('We have more questions!');
    }
  }

  @override
  Widget build(BuildContext context) {
    const userName = 'Ahmad Farel';

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Hello, $userName'),
          backgroundColor: Color.fromRGBO(242, 103, 34, 1),
        ),
        body: (_questionIndex < _questions.length)
            ? Quiz(
                answerQuestion: _answerQuestion,
                question: _questions,
                questionIndex: _questionIndex,
              )
            : Result(_totalScore, _resetPlay),
      ),
    );
  }
}
