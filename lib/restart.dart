import 'package:flutter/material.dart';

class Restart extends StatelessWidget {
  final Function resetHandler;
  Restart(this.resetHandler);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text('Again ?'),
      textColor: Color.fromRGBO(242, 103, 34, 1),
      onPressed: resetHandler,
    );
  }
}
