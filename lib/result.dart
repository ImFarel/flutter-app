import 'package:flutter/widgets.dart';

import './restart.dart';

class Result extends StatelessWidget {
  final int totalScore;
  final Function resetHandler;

  Result(this.totalScore, this.resetHandler);

  String get resultPhrase {
    print(totalScore);
    return totalScore <= 8
        ? 'You are awesome and innocent'
        : totalScore <= 12
            ? 'Pretty likeable!'
            : totalScore <= 16 ? 'You wierd :/' : 'You are so bad!';
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            resultPhrase,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 36,
              fontWeight: FontWeight.bold,
            ),
          ),
          Restart(resetHandler)
        ],
      ),
    );
  }
}
